package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.TaskRepository;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class TaskService {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private TaskRepository taskRepository = new TaskRepository();

    public void create(String taskName) {
        Task task = new Task();

        task.setName(taskName);
        taskRepository.add(task);
    }

    public List<String> list() {
        return taskRepository.list();
    }

    public boolean update(String nameOld, String nameNew) {
        return taskRepository.updateName(nameOld, nameNew);
    }

    public boolean remove(String taskName) {
        return taskRepository.remove(taskName);
    }

    public void clear() {
        taskRepository.clear();
    }

}
