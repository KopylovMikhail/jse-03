package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private TaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    public void create(String projectName) {
        Project project = new Project();

        project.setName(projectName);
        projectRepository.add(project);
    }

    public List<String> list() {
        return projectRepository.list();
    }

    public boolean update(String nameOld, String nameNew) {
        return projectRepository.updateName(nameOld, nameNew);
    }

    public boolean remove(String projectName) {
        String projectId = projectRepository.findIdByName(projectName);
        List<String> taskIdList = taskOwnerRepository.taskIdListByProjectId(projectId);

        for (String taskId : taskIdList) {
            taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
        }
        return projectRepository.remove(projectName);
    }

    public void clear() {
        List<String> allTaskIdList = taskOwnerRepository.allTaskIdList();

        for (String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.clear();
    }

    public void setTask(String projectName, String taskName) {
        String projectId = projectRepository.findIdByName(projectName);
        String taskId = taskRepository.findIdByName(taskName);

        taskOwnerRepository.setTaskToProject(projectId, taskId);
    }

    public List<String> tasksList(String projectName) {
        String projectId = projectRepository.findIdByName(projectName);
        List<String> taskIdList = taskOwnerRepository.taskIdListByProjectId(projectId);

        return taskRepository.TaskNameListByTaskId(taskIdList);
    }

}
