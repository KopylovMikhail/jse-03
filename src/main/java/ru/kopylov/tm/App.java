package ru.kopylov.tm;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public class App {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();

        bootstrap.start();
    }

}
