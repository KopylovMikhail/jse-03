package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    public static Map<String, Project> projectMap = new LinkedHashMap<>();

    public void add(Project project) {
        projectMap.put(project.getId(), project);
    }

    public List<String> list() {
        List<String> list = new ArrayList<>();

        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            list.add(entry.getValue().getName());
        }
        return list;
    }

    public boolean updateName(String nameOld, String nameNew) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (nameOld.equals(entry.getValue().getName())) {
                entry.getValue().setName(nameNew);
                return true;
            }
        }
        return false;
    }

    public boolean remove(String projectName) {
        return projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(projectName));
    }

    public void clear() {
        projectMap.clear();
    }

    public String findIdByName(String projectName) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getValue().getId();
            }
        }
        return null; //TODO: возможен NPE, необходимо будет обработать в сервисе
    }

}
