package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    public static Map<String, Task> taskMap = new LinkedHashMap<>();

    public void add(Task task) {
        taskMap.put(task.getId(), task);
    }

    public List<String> list() {
        List<String> list = new ArrayList<>();

        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            list.add(entry.getValue().getName());
        }
        return list;
    }

    public boolean updateName(String nameOld, String nameNew) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (nameOld.equals(entry.getValue().getName())) {
                entry.getValue().setName(nameNew);
                return true;
            }
        }
        return false;
    }

    public boolean remove(String taskName) {
        return taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(taskName));
    }

    public void removeById(String taskId) {
        taskMap.remove(taskId);
    }

    public void clear() {
        taskMap.clear();
    }

    public String findIdByName(String taskName) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getValue().getId();
            }
        }
        return null; //TODO: возможен NPE, необходимо будет обработать в сервисе
    }

    public List<String> TaskNameListByTaskId(List<String> taskIdList) {
        List<String> taskNameList = new ArrayList<>();

        for (String taskId : taskIdList) {
            taskNameList.add(TaskRepository.taskMap.get(taskId).getName());
        }
        return taskNameList;
    }

}
